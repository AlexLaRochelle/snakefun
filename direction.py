from enum import Enum
from random import randint


class Direction(Enum):
    INVALID = -1
    LEFT = 0
    UP = 1
    RIGHT = 2
    DOWN = 3

    @staticmethod
    def random():
        directionNum = randint(0, 3)

        switcher = {
            0: Direction.LEFT,
            1: Direction.UP,
            2: Direction.RIGHT,
            3: Direction.DOWN
        }

        return switcher.get(directionNum, "Invalid direction.")

    @staticmethod
    def GetName(direction):

        switcher = {
            -1: "INVALID",
            0: "LEFT",
            1: "UP",
            2: "RIGHT",
            3: "DOWN"
        }

        return switcher.get(direction.value, "Invalid direction.")