from grid import Grid
from direction import Direction
from cell import CellType
from position import Position
from enum import Enum
from poswithmemory import PosWithMemory
from random import randint

GAME_HEIGHT = 16
GAME_WIDTH = 32

class GameStatus(Enum):
    DEAD = -1
    PLAYING = 0
    PAUSED = 1


class Game:
    def __init__(self):
        self.grid = Grid(GAME_HEIGHT, GAME_WIDTH)
        self.score = 0
        self.lastDirection = Direction.RIGHT

        halfHeight = int(GAME_HEIGHT/2)
        halfWidth = int(GAME_WIDTH/2)

        self.snake = [PosWithMemory(Position(halfWidth, halfHeight)),
                      PosWithMemory(Position(halfWidth - 1, halfHeight)),
                      PosWithMemory(Position(halfWidth - 2, halfHeight))]

        self.UpdateSnakeInGrid()

        self.apple = PosWithMemory(Position(randint(0, GAME_WIDTH - 1), randint(0, GAME_HEIGHT - 1)))

        self.UpdateAppleInGrid()

    def GetNextPos(self, currentPos, direction):
        nextPos = Position(currentPos.x, currentPos.y)

        if direction == Direction.LEFT:
            nextPos = Position(currentPos.x - 1, currentPos.y)

        elif direction == Direction.UP:
            nextPos = Position(currentPos.x, currentPos.y - 1)

        elif direction == Direction.RIGHT:
            nextPos = Position(currentPos.x + 1, currentPos.y)

        elif direction == Direction.DOWN:
            nextPos = Position(currentPos.x, currentPos.y + 1)

        else:
            print("INVALID DIRECTION")

        return nextPos

    def Update(self, direction):

        for index, snakePart in enumerate(self.snake):
            # Remove the old snake
            self.grid.cells[snakePart.pos.y][snakePart.pos.x].cellType = CellType.VOID

            partPos = snakePart.pos

            # snake head
            if index == 0:
                nextPos = self.GetNextPos(partPos, direction)

                # going back in on itself
                if nextPos.x == self.snake[1].pos.x and nextPos.y == self.snake[1].pos.y:
                    direction = self.lastDirection
                    nextPos = self.GetNextPos(partPos, direction)

                if nextPos.x >= 0 and nextPos.x < GAME_WIDTH and nextPos.y >= 0 and nextPos.y < GAME_HEIGHT:
                    snakePart.pos = nextPos
                    self.lastDirection = direction

                    nextPosCellType = self.grid.cells[nextPos.y][nextPos.x].cellType
                    if nextPosCellType == CellType.SNAKE:
                        return GameStatus.DEAD
                    elif nextPosCellType == CellType.APPLE:
                        self.score += 1
                        lastSnakePart = self.snake[len(self.snake) - 1]
                        self.snake.append(PosWithMemory(Position(lastSnakePart.pos.x, lastSnakePart.pos.y)))
                        self.GenerateNewApple()

                else:
                    return GameStatus.DEAD

            # the rest of the snake
            else:
                snakePart.pos = Position(self.snake[index - 1].oldpos.x, self.snake[index - 1].oldpos.y)

        self.UpdateSnakeInGrid()

    def UpdateSnakeInGrid(self):
        for snakePart in self.snake:
            self.grid.cells[snakePart.pos.y][snakePart.pos.x].cellType = CellType.SNAKE
            snakePart.oldpos = Position(snakePart.pos.x, snakePart.pos.y)

    def GenerateNewApple(self):
        # Don't need this, the snake will always replace the old apple
        # self.grid.cells[self.apple.pos.y][self.apple.pos.x].cellType = CellType.VOID

        newAppleOnSnake = True

        while self.apple.pos == self.apple.oldpos or newAppleOnSnake:
            newAppleOnSnake = False

            self.apple.pos = Position(randint(0, GAME_WIDTH - 1), randint(0, GAME_HEIGHT - 1))

            for snakePart in self.snake:
                if self.apple.pos == snakePart.pos:
                    newAppleOnSnake = True
                    break

        self.apple.oldpos = Position(self.apple.pos.x, self.apple.pos.y)
        self.UpdateAppleInGrid()

    def UpdateAppleInGrid(self):
        self.grid.cells[self.apple.pos.y][self.apple.pos.x].cellType = CellType.APPLE

    def Display(self):
        for i in range(GAME_WIDTH):
            print("-", end="")

        # newline
        print("")

        for row in self.grid.cells:
            print("-", end="")

            for cell in row:
                cell.Display()

            print("-")

        for i in range(GAME_WIDTH):
            print("-", end="")

        # newline
        print("")

        print("Score: " + str(self.score))
