from game import Game, GameStatus
from time import sleep, time
from direction import Direction
from pynput.keyboard import Key, KeyCode, Listener
from queue import Queue

import threading

from os import system, name

UPDATE_RATE = 1/8

def clearConsole():
    # for windows
    if name == 'nt':
        _ = system('cls')

        # for mac and linux(here, os.name is 'posix')
    else:
        _ = system('clear')


def listen(result):
    def on_press(key):
        None

    def on_release(key):
        result.put(key)

    with Listener(
            on_press=on_press,
            on_release=on_release) as listener:
        listener.join()


def main():
    game = Game()

    hasFrameRateIssues = False
    gamePlaying = True
    inputQueue = Queue()

    inputThread = threading.Thread(target=listen, args=(inputQueue,))
    inputThread.daemon = True
    inputThread.start()

    start = time()

    while gamePlaying:

        direction = Direction.INVALID

        if not inputQueue.empty():
            readInput = inputQueue.get()

            if isinstance(readInput, KeyCode):
                readInput = readInput.char
                if readInput == "w":
                    direction = Direction.UP
                elif readInput == "a":
                    direction = Direction.LEFT
                elif readInput == "s":
                    direction = Direction.DOWN
                elif readInput == "d":
                    direction = Direction.RIGHT
            elif isinstance(readInput, Key):
                if readInput == Key.esc:
                    print("ExitedGame...")
                    sleep(3)
                    return


        if direction == Direction.INVALID:
            direction = game.lastDirection

        if game.Update(direction) != GameStatus.DEAD:
            clearConsole()

            if hasFrameRateIssues:
                print("Your computer sucks, or you're snake is very big and my algorithms suck :)")

            game.Display()

            sleepTime = UPDATE_RATE - (start - time())

            if sleepTime > 0:
                hasFrameRateIssues = False
                sleep(sleepTime)
            else:
                hasFrameRateIssues = True

            start = time()

        else:
            print("YOU DIED")
            sleep(5)
            gamePlaying = False


if __name__ == "__main__":
    main()
