from cx_Freeze import setup, Executable

base = None

executables = [Executable("main.py", base=base)]

packages = ["idna", "os", "time", "enum", "pynput.keyboard", "queue", "random"]
options = {
    'build_exe': {
        'packages':packages,
    },
}

setup(
    name = "snake",
    options = options,
    version = "0.1",
    description = '',
    executables = executables
)