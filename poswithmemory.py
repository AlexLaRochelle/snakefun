from position import Position


class PosWithMemory:
    def __init__(self, pos: Position):
        self.pos = pos
        self.oldpos = Position(pos.x, pos.y)