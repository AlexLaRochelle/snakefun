from enum import Enum
from position import Position


class CellType(Enum):
    VOID = 0
    SNAKE = 1
    APPLE = 2


class Cell:
    def __init__(self, cellType: CellType, x, y):
        self.cellType = cellType
        self.pos = Position(x, y)

    def Display(self):
        switcher = {
            CellType.VOID: " ",
            CellType.SNAKE: "#",
            CellType.APPLE: "*"
        }

        print(switcher.get(self.cellType, "Invalid Cell type"), end="")
