from cell import Cell, CellType


class Grid:
    def __init__(self, height, width):
        self.cells = []

        for i in range(height):
            self.cells.append([])
            for j in range(width):
                self.cells[i].append(Cell(CellType.VOID, i, j))
