This is a console snake game I made from scratch in python!

Play using the w a s d keys to move.

To build the executable, make sure you have cx_Freeze and idna installed:
	- pip install cx_Freeze
	- pip install idna

Then run:
	- python setup.py build

in the root directory of the project!

The executable will be in "build/an exe folder/main.exe"!